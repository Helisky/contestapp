"""
URL configuration for contestapp project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .api import apiurlpattern
from competitor.views import HomeView,DetailView,CompetitorListView
from django.conf.urls.static import static
from django.conf import settings
from competitor.pdf_views import PDFView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HomeView.as_view(), name='home'),
    path('detail/<pk>', DetailView.as_view(), name='detail'),
    path('list/', CompetitorListView.as_view(), name='list'),
    path('pdf/', PDFView.as_view(), name='pdf-view'),
    
    
]


urlpatterns += apiurlpattern


urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)