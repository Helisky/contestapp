from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)


from competitor import api_views as competitor_views


router = routers.DefaultRouter()

# accounts
# router.register(
#     r'accounts/account-bank',
#     competitor_views.CompetitorViewset, 'competitor'
# )


apiurlpattern = [
    path('api/', include(router.urls)),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/competitor/', competitor_views.CompetitorViewset.as_view({'get': 'list', 'post': 'create'}), name='competitor'),
    path('api/competitor/<int:pk>/', competitor_views.CompetitorViewset.as_view({'get': 'retrieve'}), name='competitor'),
    path('api/career/', competitor_views.CareerViewset.as_view({'get': 'list', 'post': 'create'}), name='career'),
]