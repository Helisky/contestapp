# Sistema Administrativo de Concursantes

## clonar repositorio

```bash
    git clone https://gitlab.com/Helisky/contestapp.git
```

## ir al directorio y crear el entorno virtual

```bash
    python -m venv env
```

## activar entorno virtual

```bash
    # para linux
    source/env/bin/activate
    # windows
    env/Scripts/activate.bat
```

## instalar requerimientos

```bash
    pip install -r requirements.txt
```

## Crear base de datos

## Agregar credenciales en .env file

-Agregar variables de entorno encontradas en .env.example file

## levantar la aplicacion

```bash
    ./manage.py makemigrations
    ./manage.py migrate
    ./manage.py runserver
```

## Documentacion del sistema

([localhost]:[puerto])
