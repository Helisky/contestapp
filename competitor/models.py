from django.db import models


TYPES = [
    ('LIRICA', 'LIRICA'),
    ('ÉPICA', 'ÉPICA'),
    ('DRAMÁTICA', 'DRAMÁTICA'),
]

GENRE = [
    ('MASCULINO', 'MASCULINO'),
    ('FEMENINO', 'FEMENINO'),
]


class Career(models.Model):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.upper()
        super(Career, self).save(*args, **kwargs)

    class Meta:
        db_table = 'career'
        verbose_name = 'Carrera'


# Create your models here.
class Competitor(models.Model):
    carnet = models.CharField(max_length=10)
    name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    address = models.CharField(max_length=250)
    genre = models.CharField(max_length=10, choices=GENRE)
    phone = models.CharField(max_length=20)
    birth_date = models.DateField()
    age = models.IntegerField(blank=True, null=True)
    career = models.ForeignKey(Career, on_delete=models.CASCADE, verbose_name='Carrera', related_name='competitors')
    type = models.CharField(max_length=10, choices=TYPES)
    date_to_contest = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.carnet}'
    
    def save(self, *args, **kwargs):
        self.name = self.name.upper()
        self.address = self.address.upper()
        self.last_name = self.last_name.upper()
        self.genre = self.genre.upper()
        self.type = self.type.upper()
        super(Competitor, self).save(*args, **kwargs)
        
    class Meta:
        db_table = 'competitor'
        verbose_name = 'Competidor'