from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Competitor
from django.views.generic import DetailView, ListView


# template views 
class HomeView(TemplateView):
    template_name = 'create.html'

class DetailView(DetailView):
    model = Competitor
    template_name = 'detail.html'


class CompetitorListView(TemplateView):
    template_name = 'list.html'
    