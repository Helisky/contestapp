from .models import Competitor, Career
from .serializers import CompetitorSerializer, CareerSerializer
from rest_framework import viewsets
from rest_framework import mixins


class CareerViewset(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = CareerSerializer
    queryset = Career.objects.all()

    def perform_create(self, serializer):
        serializer.save()


class CompetitorViewset(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = CompetitorSerializer
    queryset = Competitor.objects.all()

    def perform_create(self, serializer):
        serializer.save()

    def get_queryset(self):
        career = self.request.query_params.get('career', None)
        age = self.request.query_params.get('age', None)
        date_to_contest = self.request.query_params.get(
            'date_to_contest', None)
        type = self.request.query_params.get('type', None)
        queryset = Competitor.objects.all()
        
        if career is not None and len(career) > 0:
            print('career', career)
            queryset = queryset.filter(career_id=career)
            
        if age is not None and len(age) > 0: 
            print('age', age)
            queryset = queryset.filter(age=age)
            
        if date_to_contest is not None and len(date_to_contest) > 0:
            print('date_to_contest', date_to_contest)
            queryset = queryset.filter(date_to_contest=date_to_contest)
        
        if type is not None and len(type) > 0:
            print('type', type)
            queryset = queryset.filter(type=type)
        print(queryset)
        print(len(queryset))
        return queryset
