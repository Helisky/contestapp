from django import forms

class NewCompetitor(forms.Form):
    field1 = forms.CharField(max_length=100)
    field2 = forms.EmailField()
    # Agrega más campos según tus necesidades
