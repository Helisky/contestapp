from rest_framework import serializers
from datetime import timedelta, datetime, date
import calendar


def calculate_age(born):
    today = date.today()
    age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    if age < 18:
        raise serializers.ValidationError('El participante debe ser mayor de edad')
    return age 

def validateCarnet(carnet):
    is_valid = True
    message = 'Carnet invalido'
    if len(carnet) != 6:
        raise serializers.ValidationError('El carnet debe tener 6 caracteres')
    
    
    if not carnet[0:1] == 'A' and not carnet[0:1] == 'a':
        is_valid = False
    
    # get three characters is equal to 5
    if not carnet[2] == '5':
        is_valid = False
    
    if not carnet[5] in ['1','3','9']:
        is_valid = False
        
    if not is_valid:
        raise serializers.ValidationError(message)
    
    
def set_date_to_contest(carnet, type):
    date = None
    if carnet[5] == '1' and type == 'DRAMÁTICA':
        # FIVE DATES AFTER TODAY
        date =generate_date_to_contest(carnet, 1)
    if carnet[5] == '3' and type == 'ÉPICA':
        # LAST DATE OF THE MONTH
        date = generate_date_to_contest(carnet, 2)
    
    else:
        # next friday after today
        date = generate_date_to_contest(carnet, 3)
        
    return date
    
    
    
def generate_date_to_contest(today, case):
    today = datetime.now().date()
    date = None
    if case == 1:
        days = 0
        date = today 
        while days < 5:
            date += timedelta(days=1)
            if date.weekday() < 5:
                days += 1
            
            
    elif case == 2:
        date = get_last_date_from_month(today.year, today.month)
        
        
    elif case == 3:
        day = None
        date = today 
        while day != 4:
            date += timedelta(days=1)
            if date.weekday() == 4:
                print(day)
                day = 4
                
    return date





def get_last_date_from_month(year, month):
    last_day = calendar.monthrange(year, month)[1]
    last_day_date = date(year, month, last_day)

    # Restar días hasta encontrar el último día hábil (excluyendo fines de semana)
    while last_day_date.weekday() >= 5:  # 5 y 6 representan sábado y domingo
        last_day_date -= timedelta(days=1)

    return last_day_date


