# import serializers
from rest_framework import serializers
from .models import Competitor, Career
from .utils import validateCarnet, set_date_to_contest, calculate_age


class CareerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Career
        fields = '__all__'


class CompetitorSerializer(serializers.ModelSerializer):
    career_id = serializers.IntegerField(write_only=True)
    career = CareerSerializer(read_only=True)
    
    class Meta:
        model = Competitor
        fields = '__all__'

    def create(self, validated_data):
        # validate carnet
        validateCarnet(validated_data.get('carnet'))
        # generate date_to_contest
        date_to_contest = set_date_to_contest(
            validated_data.get('carnet'), validated_data.get('type'))
        # validate_age 
        age = calculate_age(validated_data.get('birth_date'))

        # create new instance of competitor
        competitor = Competitor(
            carnet=validated_data.get('carnet'),
            name=validated_data.get('name'),
            last_name=validated_data.get('last_name'),
            address=validated_data.get('address'),
            genre=validated_data.get('genre'),
            phone=validated_data.get('phone'),
            birth_date=validated_data.get('birth_date'),
            career_id=validated_data.get('career_id'),
            type=validated_data.get('type'),
            date_to_contest=date_to_contest,
            age = age
        )
        
        competitor.save()
        
        return competitor
