from django.http import HttpResponse
from django.views import View
from competitor.models import Competitor
from reportlab.lib.pagesizes import letter
from reportlab.lib import colors
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,Paragraph
from reportlab.lib.styles import getSampleStyleSheet

class PDFView(View):
    def get(self, request):
        queryset = Competitor.objects.all()
        career = request.GET.get('career', None)
        age = request.GET.get('age', None)
        date_to_contest = request.GET.get('date_to_contest', None)
        type = request.GET.get('type', None)

        if career is not None and len(career) > 0:
            queryset = queryset.filter(career_id=career)

        if age is not None and len(age) > 0:
            queryset = queryset.filter(age=age)

        if date_to_contest is not None and len(date_to_contest) > 0:
            queryset = queryset.filter(
                date_to_contest=date_to_contest)

        if type is not None and len(type) > 0:
            queryset = queryset.filter(type=type)

        details = queryset.values_list('carnet', 'name', 'last_name', 'career__name',
                                       'genre', 'phone', 'birth_date', 'type', 'date_to_contest', 'created_at')
        # dict to lists
        # Datos de ejemplo para la tabla
        data = [
            ['Carnet', 'Nombres', 'Apellidos', 'Carrera', 'Género', 'Teléfono',
                'Fecha Nacimiento', 'Género Literario', 'Fecha de Participación', 'Registrado']
        ]
        data += [list(i) for i in details]
        print(data)

        # Crea el objeto HttpResponse con el tipo de contenido PDF
        response = HttpResponse(content_type='application/pdf')

        # Establece el encabezado para que el navegador abra el archivo en el visor de PDF
        response['Content-Disposition'] = 'attachment; filename="reporte.pdf"'

        # Crea el objeto PDF
        doc = SimpleDocTemplate(response, pagesize=letter,
                                leftMargin=2.52, rightMargin=2.52,
                                topMargin=2.52, bottomMargin=2.52)

        # Establece el estilo de la tabla
        style = TableStyle([
            ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
            ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
            ('FONTSIZE', (0, 0), (-1, 0), 5),
            ('FONTSIZE', (0, 1), (-1, -1), 5),
            ('BOTTOMPADDING', (0, 0), (-1, 0), 2),
            ('BACKGROUND', (0, 1), (-1, -1), colors.beige),
            ('GRID', (0, 0), (-1, -1), 1, colors.black),
            # Ajustar el ancho de las columnas al contenido
            ('COLWIDTHS', (0, 0), (-1, -1),
             [50, 50, 20, 20, 20, 20, 20, 20, 15, 20]),
        ])

        # Crea la tabla y aplica el estilo
        table = Table(data)
        table.setStyle(style)

        # Crea el Story y agrega la tabla
         # Crea el Story con la tabla y el título
        styles = getSampleStyleSheet()
        title_style = styles['Title']
        title = Paragraph('<b>Listado de Participantes</b>', title_style)
        story = [title, table]

        # Genera el PDF
        doc.build(story)

        return response
